#!/bin/bash

echo "Running master outside Docker, please configure MQTT correctly"

echo "Setting env variables"
#export $(egrep -v '^#' nab.env | xargs) > /dev/null 2>&1

./genconf.sh

set -o allexport
[[ -f nab.env ]] && source nab.env
set +o allexport

(cd master && node master.js)