#!/bin/bash


if [ ! -f nab.env ]; then
  echo "Please define settings in nab.env (see nab.env.example)"
  exit -1
fi

./genconf.sh

# CHANGE THIS IF RUNNING SWARM IN SEVERAL HOSTS, TO POINT TO THE
# IP OF THE SWARM MANAGER, SO THAT IT IS VISIBLE TO THE WORKERS
HOST_IP=127.0.0.1

echo "Starting SWARM..."
docker swarm init --advertise-addr ${HOST_IP}


echo "Starting NAB-Cluster..."
docker stack deploy -c docker-compose.yml NAB-Cluster
