#!/bin/bash

echo "stopping swarm..."
docker swarm leave --force

#echo "stopping docker containers.."
#docker stop $(docker ps -aq)

#echo "removing docker containers.."
#docker rm $(docker ps -aq)

#Reclaim space and clear cache
# will input yes when prompted
#yes | docker system prune -a