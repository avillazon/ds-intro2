#!/bin/bash

echo "Running crawler outside Docker, please configure MQTT correctly"

echo "Setting env variables"

./genconf.sh

set -o allexport
[[ -f nab.env ]] && source nab.env
set +o allexport

(cd worker/crawler/crawler && node crawler.js)