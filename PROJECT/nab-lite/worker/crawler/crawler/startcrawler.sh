#!/bin/bash

echo "NAB_MASTER = ${NAB_MASTER}:${NAB_MASTER_PORT}"
./wait-for ${NAB_MASTER}:${NAB_MASTER_PORT} -t 1000 -- node crawler.js
echo "AFTER WAIT-FOR..."