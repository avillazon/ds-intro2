var config = require('./config'),
request = require('request'),
mqtt = require('mqtt'),
client = mqtt.connect('mqtt://' + process.env.NAB_MQTT_SERVER + ':' + process.env.NAB_MQTT_SERVER_PORT),
os = require('os'),
ip = require('ip');

var alreadyRegistered = false;

var crawled = hasbuild = 0;
var totalCrawled = 0;
var itemsCounter = 0;

var nabConfig = require(`./nabConfig`);

var id = ip.address().toString() + '-' + os.hostname().toString();
var crawlerToken = ''; 

var headers = function () {
    let TokenToUse = crawlerToken;
    return {
        'User-Agent': config.user_agent,
        'Authorization': 'token ' + TokenToUse 
    }
}

// Must verify if the limit is close to be reached, and handle it to 
// supend the crawling
function verifyRemaining(header){
    console.log("x-ratelimit-remaining : "+header['x-ratelimit-remaining']);
    console.log("x-ratelimit-reset : " + header['x-ratelimit-reset']);
}

function checkRepoBuildfile(repo){
    request({ 
        url: 'http://raw.githubusercontent.com/'+repo.full_name+'/'+repo.default_branch+'/' + nabConfig.buildtool.buildfile, 
        headers: headers()
    }, function(err, response, body){
        if (err) {
            console.log(err);
            return;
        }
        hasbuild++;
        // should send results to the master 
    });
}


// get next link from the header
function getNext(headers){
    var next;
    if (headers.link) {
        var links = headers.link.split(',');
        next = links.find(str => str.indexOf('rel="next"') > -1);
        if (next) next = next.substring(next.lastIndexOf('<')+1, next.lastIndexOf('>'));
    }
    return next;
}

function getAllReposRange(startDate, endDate){
    var startDate = startDate;
    var endDate = endDate;
    if(startDate.split('.').length > 1) {
        startDate = startDate.split('.')[0]+"Z";
    }
    if(endDate.split('.').length > 1) {
        endDate = endDate.split('.')[0]+"Z";
    }
    var url = 'https://api.github.com/search/repositories?q=language:' + nabConfig.crawl.language + '+pushed:'+ startDate +'..' + endDate +'&sort=stars&order=desc&per_page=100';
    console.log(url);
    getAllRepos(url);
}

function getAllRepos(url) {
    request({ url, headers : headers() }, function(err, response, body){
        if (err) {
            console.log(err);
            return;
        };
        var total_count  = JSON.parse(body).total_count;
        
        console.log('Total to crawl ' + total_count);
        if(typeof JSON.parse(body).items === 'undefined') {
                console.log('items are undefined... skipping');
        }else{
            itemsCounter+=JSON.parse(body).items.length;
            console.log("ITEMS_COUNTER " + itemsCounter);
            JSON.parse(body).items.forEach(repo => {
                totalCrawled++;
                console.log("CRAWLED " + totalCrawled);
                crawled++;
                checkRepoBuildfile(repo);
            });
        }
        verifyRemaining(response.headers);
        var next = getNext(response.headers);
        if (next) {
            getAllRepos(next);    
        } else {
            console.log("Send crawler stats crawled: " + crawled + " hasbuild: " + hasbuild );
            client.publish(config.availableCrawler, JSON.stringify({id, stats: { crawled, hasbuild}}));
        }
    });
}



console.log('Connecting to ' + process.env.NAB_MQTT_SERVER+ ':' + process.env.NAB_MQTT_SERVER_PORT + '...');
client.on('connect', function(){
    console.log('Connected! ', 'id: ', id);
    client.subscribe(config.crawlTask); 
    if(!alreadyRegistered) {
        alreadyRegistered = true;
        console.log('Registering Crawler! ', 'id: ', id);
        client.publish(config.availableCrawler, JSON.stringify({id}));
    }
});

client.on('message', function(topic, message){
    message = JSON.parse(message.toString());
    switch (topic)
    {
        case config.crawlTask:
            if (message.id === id) {
                console.log(message);
                crawlerToken = message.token;
                crawled = hasbuild = 0;
                console.log("NEW RANGE: " + message.fromDate + ' TO ' + message.toDate + ' TOTALCOUNT ' + message.totalCount + " TOKEN " + crawlerToken);
                getAllReposRange(message.fromDate, message.toDate);            
                
            }
            break;
    }
});
