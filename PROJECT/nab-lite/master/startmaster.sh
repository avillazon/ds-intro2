#!/bin/bash

echo "NAB_MQTT_SERVER = ${NAB_MQTT_SERVER}:${NAB_MQTT_SERVER_PORT}"
echo "Waiting for NAB_MQTT_SERVER..."
./wait-for ${NAB_MQTT_SERVER}:${NAB_MQTT_SERVER_PORT} -t 1000 -- node master.js
echo "AFTER WAIT-FOR NAB_MQTT_SERVER..."
