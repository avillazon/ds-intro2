var config = require('./config'),
request = require('request'),
fs = require('fs')

var nabConfig = require(`./nabConfig`);
//mqtt server
var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://'+process.env.NAB_MQTT_SERVER+':'+process.env.NAB_MQTT_SERVER_PORT);
var http = require('http')

var env = require('env-var');

var dateIntervals = []; // date intervals to distribute to crawlers
var projects = []; // here we store the valid projects!!
var crawlers = []; // available crawlers

var alreadyInitialized = false;
var projectsPerCrawler = process.env.NAB_PROJECTS_PER_CRAWLER;
// the tokens for crawlers
var crawlerTokenArray = process.env.NAB_CRAWLER_TOKEN.split(',');
console.log(`Number of crawler tokens: ${crawlerTokenArray.length} tokens`);
var tokenArray = process.env.NAB_MASTER_TOKEN.split(',');
console.log(`Using ${tokenArray.length} tokens`);
var tokenIndex=0;

var headers = function () {
    let TokenToUse = tokenArray[tokenIndex];
    return {
        'User-Agent': config.user_agent,
        'Authorization': 'token ' + TokenToUse 
    }
}


// Verify the remaining time in the header, and blocks until the 
// time has elapsed to the ratelimit reset time
function verifyRemaining(header){
    console.log("x-ratelimit-remaining "+header['x-ratelimit-remaining']);
    console.log("x-ratelimit-reset : "+header['x-ratelimit-reset']);
    // here you must handle the github API penalty when token requests 
    // are exceeded
}

var runID = process.env.NAB_RUN_ID;
var crawled = hasbuild = 0;

// simple HTTP server to allow wait-for to validate running master
http.createServer(function (req, res) {
  res.write('NAB MASTER READY'); 
  res.end(); 
}).listen(process.env.NAB_MASTER_PORT); 

// Connection with Broker stablished. Subscribe to necessary topics
client.on("connect", function()
{
    client.subscribe(config.availableCrawler);
    client.subscribe("results-" + runID);
    main();
})

// On message received through MQTT, implement the required handing
client.on("message", function(topic, message)
{
    console.log("Received: " + topic + " - " + message.toString())
    switch (topic)
    {
        case config.availableCrawler:
            handleAvailableCrawler(message.toString());
            break;
    }
})

function main()
{
    if(!alreadyInitialized) {
        dateIntervals = [];
        projects = [];
        crawlers = [];
        if (!processDates())
        {
            console.log("+-------------------------------------------+");
            console.log("| Master needs env var NAB_FROM and NAB_TO  |");
            console.log("+-------------------------------------------+");
            client.end();
        }
        alreadyInitialized = true;
    }
}

function getAllReposRange(fromDate, toDate){
    var fromDate = fromDate;
    var toDate = toDate;
    console.log("fromDate " + fromDate);
    console.log("toDate " + toDate);
    if(fromDate.split('.').length > 1) {
        fromDate = fromDate.split('.')[0]+"Z";
    }
    if(toDate.split('.').length > 1) {
        toDate = toDate.split('.')[0]+"Z";
    }
    // Querying the GitHub API
    var url = 'https://api.github.com/search/repositories?q=language:' + nabConfig.crawl.language + '+pushed:'+ fromDate +'..' + toDate +'&sort=stars&order=desc&per_page=100';
    console.log(url);
    getAllRepos(url, fromDate, toDate);
}

function getAllRepos(url, fromDate, toDate) {
    request({ url, headers : headers() }, function(err, response, body){
        if (err) {
            console.log(err);
            return;
        };
        var total_count = JSON.parse(body).total_count;
        console.log('TOTAL COUNT ' + total_count);

        // should check if total_count is not undefined (e.g. when token reached max time)

        // handler if the token is taking penalties...  
        verifyRemaining(response.headers);

        // should split the range until the number of entries is smaller
        // than the projectPerCrawler

        // only when reached the corrrect interval, store in the 
        // dateInterval array, to distribute crawling task.

        var entry = {fromDate : fromDate, toDate: toDate, totalCount : total_count};
        dateIntervals.push(entry); // actual range for crawler
    });
}


function generateDateIntervals(fromDate, toDate) {
    var fromDate = JSON.parse(JSON.stringify(new Date(fromDate))).split('.')[0]+"Z";
    var toDate = JSON.parse(JSON.stringify(new Date(toDate))).split('.')[0]+"Z";
    getAllReposRange(fromDate, toDate);
}

function distributeCrawlerTasks() {
    if((crawlers.length > 0) && 
        (dateIntervals.length > 0) && 
        (crawlerTokenArray.length>0)) {
        console.log('Crawling tasks can be distributed');
        console.log('Crawler: ' + crawlers[0]);
        console.log('dateIntervals: ' + JSON.stringify(dateIntervals[0]));
        console.log('Token: ' + crawlerTokenArray[0]);
        // take crawler, date interval and token, and send it to crawler...
        // should publish the task to the topic to give a task the crawler
    }else{
        console.log('No crawling task to distribute');
    }
}

function startScheduler() {
    setInterval(function(){
        console.log('Trying to distribute tasks...');
        distributeCrawlerTasks();
    }, 5000);
}

function processDates()
{
    if(process.env.NAB_FROM == undefined || process.env.NAB_TO == undefined)
    {
        return false;
    }
    console.log("FROM : " + process.env.NAB_FROM);
    console.log("TO : " + process.env.NAB_TO);
    var sDate = new Date(process.env.NAB_FROM);
    var eDate = new Date(process.env.NAB_TO);
    if (sDate.getTime() > eDate.getTime())
    {
        return false;
    }
    generateDateIntervals(sDate, eDate);
    startScheduler();
    return true;
}


function handleAvailableCrawler(message) {
    message = JSON.parse(message);
    var worker = message.id;

    // insert in the list of available crawlers
    if (crawlers.indexOf(worker) == -1)
    {
        crawlers.push(worker);
    }

    // is stats available, save in DB
    if(message.stats) {
        crawled += message.stats.crawled;
        hasbuild += message.stats.hasbuild;
        saveToDB("crawlerStats", message, "Save crawler stats " + JSON.stringify(message));
    }
    
}

// Simulates saving to DB...
function saveToDB(collection, data, consoleMessage) {
    console.log('Should save in DB');
    console.log('collection: ' + collection);
    console.log('data: ' + data);
    console.log('consoleMessage: ' + consoleMessage);
}

