//var isInDocker = require(`is-in-docker`);
var isDocker = require(`is-docker`);

var nabConfig = {};
if(isDocker()) {
    // adapts to envs as passed in Docker
    nabConfig = JSON.parse(process.env.NAB_CONFIG.replace(/\\\\\\/g,"\\").slice(1, -1).replace(/\\\"/g,"\""));
}else{
    nabConfig = JSON.parse(process.env.NAB_CONFIG);
}

module.exports = nabConfig;
