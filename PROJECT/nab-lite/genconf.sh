#!/bin/bash

jqbin=`command -v jq`

if [ -z ${jqbin} ]; then
    echo "Install jq https://stedolan.github.io/jq/ requires version 1.5 or higher"
    exit -1
fi

if [ ! -f nab-config.json ]; then
  echo "Please set the NAB conf nab-config.json for the experiment (see nab-config.json.example)"
  exit -1
fi

echo "Generating NAB_CONFIG for nab.env"
echo "Removing existing entry in nab.env"
if [ `uname -s` == "Darwin" ]; then
    sed -i '' '/NAB_CONFIG/d' nab.env
else
    sed -i '/NAB_CONFIG/d' nab.env
fi


CONFIGJSON=`jq -c . < nab-config.json | jq -sR '.' | sed 's/\\\n//g'`
echo "NAB_CONFIG=${CONFIGJSON}" >> nab.env

echo "Generating NAB_RUN_ID..."
time_t=$(date +%s)
time_t_ms=$(($time_t * 1000))
hexstamp=$(printf "%x" $time_t_ms)

sed -i -e "s/NAB_RUN_ID.*/NAB_RUN_ID=NAB$hexstamp/g" nab.env

if [ `uname -s` == "Darwin" ]; then
    rm nab.env-e
fi
